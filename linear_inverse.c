#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define A(i,j) a[(i) + (j)*n]
#define AP(i,j) ap[(i) + (j)*n]
#define m1(i,j) mat1[(i)+(j)*n]
#define m2(i,j) mat2[(i)+(j)*n]

void dgesv_();
void dgemm_();

int main()
{
	int i,j,n=5,info,one=1;
	double *a=malloc(n*n*sizeof(double));
	double *ap=malloc(n*n*sizeof(double));
	double *mat1=malloc(n*n*sizeof(double));
	double *mat2=malloc(n*n*sizeof(double));
	for(i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
		{
			A(i,j)=1.0/(i+j+1);
			if(i==j)
			{
				AP(i,j)=1;
			}
		}
	}
	int *ipiv=malloc(n*sizeof(int));
	
	dgesv_(&n,&n,a,&n,ipiv,ap,&n,&info);
	for(i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
		{
			A(i,j)=1.0/(i+j+1);
		}
	}
	printf("\nMatrix A\n\n");
	for(i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
		{
			printf("%16.6E\t",A(i,j));
		}
		printf("\n");
	}
	printf("\nInverse A\n\n");
	for(i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
		{
			printf("%16.6E\t",AP(i,j));
		}
		printf("\n");
	}
	printf("\nInv(A)*A\n\n");
	for(i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
		{
			double sum1=0;
			double sum2=0;
			int k;
			for(k=0;k<n;k++)
			{
				sum1=AP(i,k)*A(k,j)+sum1;
				sum2=A(i,k)*AP(k,j)+sum2;
			}
			m1(i,j)=sum1;
			m2(i,j)=sum2;
		}
	}
	for(i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
	 	{
	 		printf("%16.6E\t",m1(i,j));
	 	}	
		printf("\n");
	}
	printf("\nA*Inv(A)\n\n");
	for(i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
		{
			printf("%16.6E\t",m2(i,j));
		}
		printf("\n");
	}


}
