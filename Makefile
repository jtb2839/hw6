CC:=icc
 
linear_equation: linear_equation.o
	$(CC) -o $@ $^ -L$$TACC_MKL_LIB/ -lmkl_intel_lp64 -lmkl_core -lmkl_sequential -lpthread
linear_inverse: linear_inverse.o
	$(CC) -o $@ $^ -L$$TACC_MKL_LIB/ -lmkl_intel_lp64 -lmkl_core -lmkl_sequential -lpthread -g
linear_equation.o: linear_equation.c
	$(CC) -c $<
linear_inverse.o: linear_inverse.c
	$(CC) -c $<

all: linear_inverse linear_equation
